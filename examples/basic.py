#!/usr/bin/env python3

import pprint
import sys
import time

import hydrus.utils

NAME = "Test"
REQUIRED_PERMISSIONS = [
    hydrus.Permission.ImportURLs,
    hydrus.Permission.ImportFiles,
    hydrus.Permission.AddTags,
    hydrus.Permission.SearchFiles,
    hydrus.Permission.ManagePages,
]
ERROR_EXIT_CODE = 1

client = hydrus.Client()
print("Client API version: v{} | Endpoint API version: v{}".format(client.VERSION, client.api_version))

api_key = hydrus.utils.cli_request_api_key(NAME, REQUIRED_PERMISSIONS)
client = hydrus.Client(api_key)
if not hydrus.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
    print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
    sys.exit(ERROR_EXIT_CODE)

url_info = client.get_url_info("https://hydrusnetwork.github.io/hydrus/help/client_api.html")
print(url_info)

all_file_ids = client.search_files(["test"])
for file_ids in hydrus.utils.yield_chunks(all_file_ids, 100):
    pprint.pprint(client.file_metadata(file_ids=file_ids))

print(client.session_key())
for page in client.get_pages(flatten=True):
    if page["name"] == "top page notebook":
        continue

    print(page)
    client.focus_page(page["page_key"])
    pprint.pprint(client.get_page_info(page["page_key"]))
    time.sleep(1)
